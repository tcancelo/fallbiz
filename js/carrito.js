document.addEventListener('DOMContentLoaded', function () {
    const botonesAgregar = document.querySelectorAll('.agregar-carrito');
    const listaCarrito = document.getElementById('lista-carrito');
    const totalCarrito = document.getElementById('total-carrito');
    let carrito = JSON.parse(localStorage.getItem('carrito')) || [];
    let total = JSON.parse(localStorage.getItem('total')) || 0;

    botonesAgregar.forEach((boton) => {
        boton.addEventListener('click', agregarAlCarrito);
    });

    actualizarCarrito();

    function agregarAlCarrito(event) {
        event.preventDefault();

        const elemento = event.target.parentElement;
        const nombre = elemento.querySelector('p').innerText;
        const precio = Number(elemento.querySelector('.precio').innerText.replace('$', '').replace(',', ''));

        const itemExistente = carrito.find(item => item.nombre === nombre);

        if (itemExistente) {
            itemExistente.cantidad++;
        } else {
            carrito.push({ nombre, precio, cantidad: 1 });
        }

        total += precio;
        totalCarrito.innerText = total.toFixed(2);

        localStorage.setItem('carrito', JSON.stringify(carrito));
        localStorage.setItem('total', JSON.stringify(total));

        actualizarCarrito();
    }

    function actualizarCarrito() {
        listaCarrito.innerHTML = '';

        carrito.forEach(item => {
            const li = document.createElement('li');
            li.textContent = `${item.nombre} - Cantidad: ${item.cantidad} - $${item.precio * item.cantidad}`;
            listaCarrito.appendChild(li);
        });
    }
});
